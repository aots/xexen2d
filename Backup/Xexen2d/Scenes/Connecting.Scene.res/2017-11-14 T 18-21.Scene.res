﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="424012204">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3673098210">
        <_items dataType="Array" type="Duality.Component[]" id="4242101904" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="481289422">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">424012204</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-200</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-200</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="1970398681">
            <active dataType="Bool">true</active>
            <farZ dataType="Float">10000</farZ>
            <focusDist dataType="Float">245</focusDist>
            <gameobj dataType="ObjectRef">424012204</gameobj>
            <nearZ dataType="Float">0</nearZ>
            <passes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Camera+Pass]]" id="2858751997">
              <_items dataType="Array" type="Duality.Components.Camera+Pass[]" id="2064157990" length="4">
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="111893760">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="All" value="3" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="PerspectiveWorld" value="0" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="AllGroups" value="2147483647" />
                </item>
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="986190286">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="None" value="0" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="OrthoScreen" value="1" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
            </passes>
            <perspective dataType="Enum" type="Duality.Drawing.PerspectiveMode" name="Parallax" value="1" />
            <priority dataType="Int">0</priority>
            <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1710146698" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3406981496">
            <item dataType="Type" id="824457068" value="Duality.Components.Transform" />
            <item dataType="Type" id="912884790" value="Duality.Components.Camera" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2261603294">
            <item dataType="ObjectRef">481289422</item>
            <item dataType="ObjectRef">1970398681</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">481289422</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3422527524">D7bQ5Swxz0K4s8TDiLxu+A==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Camera</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2006912610">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3114621100">
        <_items dataType="Array" type="Duality.Component[]" id="110846692" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2064189828">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2006912610</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">100</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">100</Z>
            </posAbs>
            <scale dataType="Float">2.6</scale>
            <scaleAbs dataType="Float">2.6</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="3475531890">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2006912610</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">320</H>
              <W dataType="Float">480</W>
              <X dataType="Float">-240</X>
              <Y dataType="Float">-160</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Xexen2d\hexen4.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3136429494" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="4204798182">
            <item dataType="ObjectRef">824457068</item>
            <item dataType="Type" id="3962226048" value="Duality.Components.Renderers.SpriteRenderer" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="870381882">
            <item dataType="ObjectRef">2064189828</item>
            <item dataType="ObjectRef">3475531890</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2064189828</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2928993894">+nDmf2G0lEKHqxRBNQWEFw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">hexen4</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="928994742">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="223408288">
        <_items dataType="Array" type="Duality.Component[]" id="2964670684" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="986271960">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">928994742</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-160</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">0</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-160</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">0</Z>
            </posAbs>
            <scale dataType="Float">2</scale>
            <scaleAbs dataType="Float">2</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="SnowyPeak.Duality.Plugin.Frozen.UI.Widgets.ListBox" id="2140939356">
            <_activeArea dataType="Enum" type="SnowyPeak.Duality.Plugin.Frozen.UI.ActiveArea" name="Center" value="6" />
            <_buttonSize dataType="Struct" type="Duality.Vector2" />
            <_clipRect dataType="Struct" type="Duality.Rect" />
            <_cursorSize dataType="Struct" type="Duality.Vector2" />
            <_customButtonMinusAppearance />
            <_customButtonPlusAppearance />
            <_customCursorAppearance />
            <_customHighlightAppearance />
            <_customScrollBarAppearance />
            <_customWidgetAppearance />
            <_items dataType="Struct" type="System.Collections.Generic.List`1[[System.Object]]" id="450770572">
              <_items dataType="Array" type="System.Object[]" id="2187579812" length="0" />
              <_size dataType="Int">0</_size>
            </_items>
            <_next />
            <_previous />
            <_rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">200</H>
              <W dataType="Float">160</W>
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
            </_rect>
            <_scrollSpeed dataType="Int">5</_scrollSpeed>
            <_skin dataType="Struct" type="Duality.ContentRef`1[[SnowyPeak.Duality.Plugin.Frozen.UI.Resources.Skin]]">
              <contentPath dataType="String">Frozen\UI\FrozenUISkin</contentPath>
            </_skin>
            <_status dataType="Enum" type="SnowyPeak.Duality.Plugin.Frozen.UI.Widgets.Widget+WidgetStatus" name="Normal" value="1" />
            <_textColor dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </_textColor>
            <_textFont dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
              <contentPath dataType="String">Data\Xexen2d\Main.Font.res</contentPath>
            </_textFont>
            <_tint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">112</A>
              <B dataType="Byte">0</B>
              <G dataType="Byte">0</G>
              <R dataType="Byte">0</R>
            </_tint>
            <_visiblityFlag dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
            <_zOffset dataType="Float">0</_zOffset>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">928994742</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="278323342" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="483738738">
            <item dataType="ObjectRef">824457068</item>
            <item dataType="Type" id="3633468624" value="SnowyPeak.Duality.Plugin.Frozen.UI.Widgets.ListBox" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3126588490">
            <item dataType="ObjectRef">986271960</item>
            <item dataType="ObjectRef">2140939356</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">986271960</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2802180930">kPsAk/0hj0ymndKgprMuuw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">ListBox</name>
      <parent />
      <prefabLink />
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
