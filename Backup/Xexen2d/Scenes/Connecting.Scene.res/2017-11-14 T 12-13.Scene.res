﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="3960603310">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2820831224">
        <_items dataType="Array" type="Duality.Component[]" id="556081516" length="4">
          <item dataType="Struct" type="Xexen2d.Network.NetworkSync" id="3408780927">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3960603310</gameobj>
          </item>
        </_items>
        <_size dataType="Int">1</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2945483230" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2360953402">
            <item dataType="Type" id="4052888832" value="Xexen2d.Network.NetworkSync" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2228114106">
            <item dataType="ObjectRef">3408780927</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2971582778">lRJhKQN5BEWLOqG2rqkAkQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">NetworkSync</name>
      <parent />
      <prefabLink />
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
