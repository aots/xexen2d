﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="2929938994">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3918794716">
        <_items dataType="Array" type="Duality.Component[]" id="1716996804" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2987216212">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2929938994</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">100</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">100</Z>
            </posAbs>
            <scale dataType="Float">2.6</scale>
            <scaleAbs dataType="Float">2.6</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="103590978">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2929938994</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">320</H>
              <W dataType="Float">480</W>
              <X dataType="Float">-240</X>
              <Y dataType="Float">-160</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Xexen2d\hexen4.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1302149398" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3931916534">
            <item dataType="Type" id="2349368544" value="Duality.Components.Transform" />
            <item dataType="Type" id="2615834510" value="Duality.Components.Renderers.SpriteRenderer" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1665703450">
            <item dataType="ObjectRef">2987216212</item>
            <item dataType="ObjectRef">103590978</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2987216212</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3035223574">r85ZIAj4UESm2bUS0EydqQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">hexen4</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1153139549">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="362489199">
        <_items dataType="Array" type="Duality.Component[]" id="374197742" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="1210416767">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1153139549</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-200</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-200</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="2699526026">
            <active dataType="Bool">true</active>
            <farZ dataType="Float">10000</farZ>
            <focusDist dataType="Float">245</focusDist>
            <gameobj dataType="ObjectRef">1153139549</gameobj>
            <nearZ dataType="Float">0</nearZ>
            <passes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Camera+Pass]]" id="3737561870">
              <_items dataType="Array" type="Duality.Components.Camera+Pass[]" id="673930704" length="8">
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="2772016828">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="All" value="3" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="PerspectiveWorld" value="0" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="AllGroups" value="2147483647" />
                </item>
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="3629429398">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="None" value="0" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="OrthoScreen" value="1" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
            </passes>
            <perspective dataType="Enum" type="Duality.Drawing.PerspectiveMode" name="Parallax" value="1" />
            <priority dataType="Int">0</priority>
            <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3916270240" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2650297029">
            <item dataType="ObjectRef">2349368544</item>
            <item dataType="Type" id="80495318" value="Duality.Components.Camera" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="913187368">
            <item dataType="ObjectRef">1210416767</item>
            <item dataType="ObjectRef">2699526026</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1210416767</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1954015567">3XJ38bpr9kSaLgEebytc0g==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Camera</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1836818736">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2039099030">
        <_items dataType="Array" type="Duality.Component[]" id="3624053280" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="1894095954">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1836818736</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-160.9375</X>
              <Y dataType="Float">145.3125</Y>
              <Z dataType="Float">0</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-160.9375</X>
              <Y dataType="Float">145.3125</Y>
              <Z dataType="Float">0</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="SnowyPeak.Duality.Plugin.Frozen.UI.Widgets.TextBox" id="826871813">
            <_activeArea dataType="Enum" type="SnowyPeak.Duality.Plugin.Frozen.UI.ActiveArea" name="Center" value="6" />
            <_clipRect dataType="Struct" type="Duality.Rect" />
            <_customWidgetAppearance dataType="String"></_customWidgetAppearance>
            <_keyRepeatSpeed dataType="Float">0.2</_keyRepeatSpeed>
            <_next />
            <_previous />
            <_rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">300</W>
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
            </_rect>
            <_skin dataType="Struct" type="Duality.ContentRef`1[[SnowyPeak.Duality.Plugin.Frozen.UI.Resources.Skin]]">
              <contentPath dataType="String">Frozen\UI\FrozenUISkin</contentPath>
            </_skin>
            <_status dataType="Enum" type="SnowyPeak.Duality.Plugin.Frozen.UI.Widgets.Widget+WidgetStatus" name="Normal" value="1" />
            <_text dataType="String"></_text>
            <_textColor dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">250</B>
              <G dataType="Byte">254</G>
              <R dataType="Byte">239</R>
            </_textColor>
            <_textFont dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
              <contentPath dataType="String">Data\Xexen2d\Main.Font.res</contentPath>
            </_textFont>
            <_tint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">183</B>
              <G dataType="Byte">183</G>
              <R dataType="Byte">183</R>
            </_tint>
            <_visiblityFlag dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
            <_zOffset dataType="Float">0</_zOffset>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1836818736</gameobj>
          </item>
          <item dataType="Struct" type="Xexen2d.StupidTextInput" id="596482078">
            <_x003C_NextScene_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Scene]]">
              <contentPath dataType="String">Data\Xexen2d\Scenes\Connecting.Scene.res</contentPath>
            </_x003C_NextScene_x003E_k__BackingField>
            <_x003C_TextBox_x003E_k__BackingField dataType="ObjectRef">826871813</_x003C_TextBox_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1836818736</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3455511770" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3115328100">
            <item dataType="ObjectRef">2349368544</item>
            <item dataType="Type" id="2480176580" value="SnowyPeak.Duality.Plugin.Frozen.UI.Widgets.TextBox" />
            <item dataType="Type" id="1685621142" value="Xexen2d.StupidTextInput" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="758863894">
            <item dataType="ObjectRef">1894095954</item>
            <item dataType="ObjectRef">826871813</item>
            <item dataType="ObjectRef">596482078</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1894095954</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="639510368">OUS0JNfsjUuNEQzFyvJ25w==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">TextBox</name>
      <parent />
      <prefabLink />
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
