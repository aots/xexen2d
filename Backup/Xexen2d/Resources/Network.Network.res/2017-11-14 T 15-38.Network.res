﻿<root dataType="Struct" type="Xexen2d.Networking.Network" id="129723834">
  <_x003C_ConnectKey_x003E_k__BackingField dataType="String">MyGame</_x003C_ConnectKey_x003E_k__BackingField>
  <_x003C_IPAddress_x003E_k__BackingField dataType="String">localhost</_x003C_IPAddress_x003E_k__BackingField>
  <_x003C_Port_x003E_k__BackingField dataType="Int">1488</_x003C_Port_x003E_k__BackingField>
  <assetInfo />
  <Client dataType="Struct" type="LiteNetLib.NetManager" id="427169525">
    <_flowModes dataType="Struct" type="System.Collections.Generic.List`1[[LiteNetLib.NetManager+FlowMode]]" id="1100841590">
      <_items dataType="Array" type="LiteNetLib.NetManager+FlowMode[]" id="2824927200" length="0" />
      <_size dataType="Int">0</_size>
    </_flowModes>
    <_logicThread dataType="Struct" type="LiteNetLib.NetThread" id="649525530">
      <_callback dataType="Delegate" type="System.Action" id="411997508" multi="true">
        <method dataType="MemberInfo" id="3760513604" value="M:LiteNetLib.NetManager:UpdateLogic" />
        <target dataType="ObjectRef">427169525</target>
        <invocationList dataType="Array" type="System.Delegate[]" id="3884632726">
          <item dataType="ObjectRef">411997508</item>
        </invocationList>
      </_callback>
      <_name dataType="String">LogicThread</_name>
      <_running dataType="Bool">false</_running>
      <_thread />
      <SleepTime dataType="Int">15</SleepTime>
    </_logicThread>
    <_maxConnections dataType="Int">1</_maxConnections>
    <_netEventListener />
    <_netEventsPool dataType="Struct" type="System.Collections.Generic.Stack`1[[LiteNetLib.NetManager+NetEvent]]" id="3234930070">
      <_array dataType="Array" type="LiteNetLib.NetManager+NetEvent[]" id="628691392" length="0" />
      <_size dataType="Int">0</_size>
    </_netEventsPool>
    <_netEventsQueue dataType="Struct" type="System.Collections.Generic.Queue`1[[LiteNetLib.NetManager+NetEvent]]" id="1841287930">
      <_array dataType="Array" type="LiteNetLib.NetManager+NetEvent[]" id="833611236" length="0" />
      <_head dataType="Int">0</_head>
      <_size dataType="Int">0</_size>
      <_tail dataType="Int">0</_tail>
    </_netEventsQueue>
    <_netPacketPool dataType="Struct" type="LiteNetLib.NetPacketPool" id="507227318">
      <_pool dataType="Struct" type="System.Collections.Generic.Stack`1[[LiteNetLib.NetPacket]]" id="2515918240">
        <_array dataType="Array" type="LiteNetLib.NetPacket[]" id="814428892" length="0" />
        <_size dataType="Int">0</_size>
      </_pool>
    </_netPacketPool>
    <_peers dataType="Struct" type="LiteNetLib.NetPeerCollection" id="1333838426">
      <_count dataType="Int">0</_count>
      <_peersArray dataType="Array" type="LiteNetLib.NetPeer[]" id="4054115332" length="1" />
      <_peersDict dataType="Struct" type="System.Collections.Generic.Dictionary`2[[LiteNetLib.NetEndPoint],[LiteNetLib.NetPeer]]" id="1120337814" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2547482382" length="0" />
          <values dataType="Array" type="System.Object[]" id="477649226" length="0" />
        </body>
      </_peersDict>
    </_peers>
    <_socket dataType="Struct" type="LiteNetLib.NetSocket" id="3865867222">
      <_onMessageReceived dataType="Delegate" type="LiteNetLib.NetManager+OnMessageReceived" id="456433792" multi="true">
        <method dataType="MemberInfo" id="3477651868" value="M:LiteNetLib.NetManager:ReceiveLogic(System.Byte[],System.Int32,System.Int32,LiteNetLib.NetEndPoint)" />
        <target dataType="ObjectRef">427169525</target>
        <invocationList dataType="Array" type="System.Delegate[]" id="964911126">
          <item dataType="ObjectRef">456433792</item>
        </invocationList>
      </_onMessageReceived>
      <_port dataType="Int">0</_port>
      <_running dataType="Bool">false</_running>
      <_threadv4 />
      <_threadv6 />
      <_udpSocketv4 />
      <_udpSocketv6 />
    </_socket>
    <_x003C_BytesReceived_x003E_k__BackingField dataType="ULong">0</_x003C_BytesReceived_x003E_k__BackingField>
    <_x003C_BytesSent_x003E_k__BackingField dataType="ULong">0</_x003C_BytesSent_x003E_k__BackingField>
    <_x003C_PacketsReceived_x003E_k__BackingField dataType="ULong">0</_x003C_PacketsReceived_x003E_k__BackingField>
    <_x003C_PacketsSent_x003E_k__BackingField dataType="ULong">0</_x003C_PacketsSent_x003E_k__BackingField>
    <ConnectKey dataType="String">MyGame</ConnectKey>
    <DisconnectTimeout dataType="Long">5000</DisconnectTimeout>
    <DiscoveryEnabled dataType="Bool">false</DiscoveryEnabled>
    <MaxConnectAttempts dataType="Int">10</MaxConnectAttempts>
    <MergeEnabled dataType="Bool">false</MergeEnabled>
    <NatPunchEnabled dataType="Bool">false</NatPunchEnabled>
    <NatPunchModule dataType="Struct" type="LiteNetLib.NatPunchModule" id="3334750266">
      <_natPunchListener />
      <_netBase dataType="ObjectRef">427169525</_netBase>
      <_requestEvents dataType="Struct" type="System.Collections.Generic.Queue`1[[LiteNetLib.NatPunchModule+RequestEventData]]" id="1007395492">
        <_array dataType="Array" type="LiteNetLib.NatPunchModule+RequestEventData[]" id="724502724" length="0" />
        <_head dataType="Int">0</_head>
        <_size dataType="Int">0</_size>
        <_tail dataType="Int">0</_tail>
      </_requestEvents>
      <_successEvents dataType="Struct" type="System.Collections.Generic.Queue`1[[LiteNetLib.NatPunchModule+SuccessEventData]]" id="2290430742">
        <_array dataType="Array" type="LiteNetLib.NatPunchModule+SuccessEventData[]" id="3432665326" length="0" />
        <_head dataType="Int">0</_head>
        <_size dataType="Int">0</_size>
        <_tail dataType="Int">0</_tail>
      </_successEvents>
    </NatPunchModule>
    <PingInterval dataType="Int">1000</PingInterval>
    <ReconnectDelay dataType="Int">500</ReconnectDelay>
    <ReuseAddress dataType="Bool">false</ReuseAddress>
    <SimulateLatency dataType="Bool">false</SimulateLatency>
    <SimulatePacketLoss dataType="Bool">false</SimulatePacketLoss>
    <SimulationMaxLatency dataType="Int">100</SimulationMaxLatency>
    <SimulationMinLatency dataType="Int">30</SimulationMinLatency>
    <SimulationPacketLossChance dataType="Int">10</SimulationPacketLossChance>
    <UnconnectedMessagesEnabled dataType="Bool">false</UnconnectedMessagesEnabled>
    <UnsyncedEvents dataType="Bool">false</UnsyncedEvents>
  </Client>
  <Listener />
</root>
<!-- XmlFormatterBase Document Separator -->
