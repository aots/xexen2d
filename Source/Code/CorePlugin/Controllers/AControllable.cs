﻿using Duality;
using Xexen2d.Utils.Packets;

namespace Xexen2d
{
    public abstract class AControllable : Component, ICmpInitializable
    {
        public virtual ControlStatePacket ControlState { get; set; }

        public virtual void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                ControlState = new ControlStatePacket();
            }
        }

        public virtual void OnShutdown(ShutdownContext context)
        {
        }
    }
}