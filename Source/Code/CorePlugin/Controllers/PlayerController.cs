﻿using Duality;
using Duality.Components.Renderers;
using Duality.Input;
using Duality.Components;
using Duality.Components.Physics;
using Xexen2d.Utils.Packets;
using Duality.Resources;
using System;
using Xexen2d.Bullets;
using Xexen2d.Attributes;

namespace Xexen2d
{
    [RequiredComponent(typeof(RigidBody)), RequiredComponent(typeof(Transform))]
    public class PlayerController : AControllable, ICmpCollisionListener, ICmpUpdatable
    {
        private bool _canJump = true;
        private float _jumpStartY;
        private bool _jumpEnded = false;
        public float AttackSpeed { get; set; } = 1f;
        private float _lastAttack;
        public float MaxSpeed { get; set; } = 10f;
        public float MaxJumpHeight { get; set; } = 10f;
        public float JumpForce { get; set; } = 100f;
        public AnimSpriteRenderer Animation { get; set; }
        private RigidBody _body;
        public Transform Transform { get; set; }
        private ControlStatePacket _state = new ControlStatePacket();
        public ContentRef<Prefab> Bullet { get; set; }

        public override ControlStatePacket ControlState
        {
            get
            {
                return _state;
            }

            set
            {
                if (value.Equals(_state))
                    return;

                if (Animation != null)
                {
                    var state = value.ControlState;
                    switch (state)
                    {
                        case EControlState.Idle:
                            Animation.AnimPaused = true;
                            break;

                        case EControlState.MoveLeft:
                            Animation.Flip = SpriteRenderer.FlipMode.Horizontal;
                            Animation.AnimPaused = false;
                            break;

                        case EControlState.MoveRight:
                            Animation.Flip = SpriteRenderer.FlipMode.None;
                            Animation.AnimPaused = false;
                            break;
                    }
                }
                //_state = (ControlStatePacket)value.Clone();
                _state = value;
            }
        }

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
            if (context == InitContext.Activate)
            {
                Animation = GameObj.GetComponent<AnimSpriteRenderer>();
                _body = GameObj.GetComponent<RigidBody>();
                Transform = GameObj.GetComponent<Transform>();
                _lastAttack = 0f;
            }
        }

        public void OnUpdate()
        {
            if (ControlState.Jumping)
            {
                if (_canJump)
                {
                    _jumpStartY = Transform.Pos.Y;
                    _canJump = false;
                }

                if (Transform.Pos.Y > _jumpStartY - MaxJumpHeight && !_jumpEnded)
                {
                    _body.ApplyLocalForce(-Vector2.UnitY * JumpForce);
                }
                else
                    _jumpEnded = true;
            }

            if (ControlState.Shooting)
            {
                if (Time.MainTimer.TotalSeconds >= _lastAttack + 1 / AttackSpeed * Time.TimeMult)
                {
                    _lastAttack = (float)Time.MainTimer.TotalSeconds;
                    var bullet = Bullet.Res.Instantiate(Transform.Pos);
                    switch (Animation.Flip)
                    {
                        case (SpriteRenderer.FlipMode.None):
                            bullet.Transform.Pos = Transform.Pos + Vector3.UnitX * 20;
                            break;

                        case (SpriteRenderer.FlipMode.Horizontal):
                            bullet.Transform.Angle = MathF.Pi;
                            bullet.Transform.Pos = Transform.Pos - Vector3.UnitX * 20;
                            break;
                    }

                    bullet.GetComponent<PlayerBullet>().Direction = Vector2.UnitX;
                    Scene.Current.AddObject(bullet);
                }
            }

            var state = ControlState.ControlState;
            switch (state)
            {
                case EControlState.MoveLeft:
                    _body.LinearVelocity = -Vector2.UnitX * MaxSpeed;
                    break;

                case EControlState.MoveRight:
                    _body.LinearVelocity = Vector2.UnitX * MaxSpeed;
                    break;

                default:
                    _body.LinearVelocity = Vector2.Zero;
                    break;
            }
        }

        public void OnCollisionBegin(Component sender, CollisionEventArgs args)
        {
            _canJump = true;
            _jumpEnded = false;
        }

        public void OnCollisionEnd(Component sender, CollisionEventArgs args)
        {
        }

        public void OnCollisionSolve(Component sender, CollisionEventArgs args)
        {
        }
    }
}