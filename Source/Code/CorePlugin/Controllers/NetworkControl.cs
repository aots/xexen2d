﻿using System;
using Duality;
using LiteNetLib;
using Xexen2d.Networking;
using Xexen2d.Utils.Packets;
using Duality.Resources;

namespace Xexen2d.Input
{
    [RequiredComponent(typeof(PlayerController))]
    public class NetworkControl : NetworkClient
    {
        public PlayerController Controller { get; set; }
        public long ID { get; set; }

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
            if (context == InitContext.Activate)
            {
                Controller = GameObj.GetComponent<PlayerController>();
                Network.Res.ControlStatePacketRecieved += ControlStatePacketRevieced;
                Network.Res.PositionPacketRecieved += PositionReceived;
                Network.Res.DisconnectRecieved += DisconnectReceived;
                //Network.Res.PlayerShotPacketRecieved += ShotRecieved;
            }
        }

        //private void ShotRecieved(object sender, PlayerShotPacket packet)
        //{
        //    if (packet.ID != ID)
        //        return;
        //    var shot = new
        //}

        private void DisconnectReceived(object sender, DisconnectPacket packet)
        {
            if (packet.ID != ID)
                return;
            Scene.Current.RemoveObject(GameObj);
        }

        private void PositionReceived(object sender, PositionPacket packet)
        {
            if (packet.ID != ID)
                return;
            Controller.Transform.Pos = new Vector3(packet.X, packet.Y, 0);
        }

        private void ControlStatePacketRevieced(object sender, ControlStatePacket packet)
        {
            if (packet.ID != ID)
                return;
            //Log.Core.Write($"-----state recv = {packet.ControlState.ToString()}");
            Controller.ControlState = packet;
        }
    }
}