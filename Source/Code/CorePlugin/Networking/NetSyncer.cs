﻿using Duality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xexen2d.Attributes;
using Duality.Resources;
using LiteNetLib.Utils;
using Xexen2d.Utils.Packets;

namespace Xexen2d.Networking
{
    [DontSerialize]
    public class NetSyncer : Component, ICmpInitializable, ICmpUpdatable
    {
        public long ID { get; set; }

        private Dictionary<ESyncType, Dictionary<PropertyInfo, INetSyncable>> PrevSyncDictionary { get; set; } = new Dictionary<ESyncType, Dictionary<PropertyInfo, INetSyncable>>();
        private Dictionary<ESyncType, Dictionary<PropertyInfo, INetSyncable>> SyncDictionary { get; set; } = new Dictionary<ESyncType, Dictionary<PropertyInfo, INetSyncable>>();
        private NetworkUpdater NetworkClient { get; set; }
        private NetDataWriter Writer { get; set; } = new NetDataWriter();
        private NetSerializer Serializer { get; set; } = new NetSerializer();

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                NetworkClient = Scene.Current.FindComponent<NetworkUpdater>();
                foreach (ESyncType type in Enum.GetValues(typeof(ESyncType)))
                {
                    SyncDictionary.Add(type, new Dictionary<PropertyInfo, INetSyncable>());
                    PrevSyncDictionary.Add(type, new Dictionary<PropertyInfo, INetSyncable>());
                }
                foreach (var comp in GameObj.GetComponents<Component>())
                {
                    AddComponentProps(comp);
                }
                GameObj.EventComponentAdded += GameObj_EventComponentAdded;
                GameObj.EventComponentRemoving += GameObj_EventComponentRemoving;
            }
        }

        private void AddComponentProps(Component comp)
        {
            var props = from p in comp.GetType().GetProperties()
                        let defined = p.IsDefined(typeof(Synced), true) && typeof(INetSyncable).IsAssignableFrom(p.PropertyType)
                        where defined
                        select p;
            foreach (var prop in props)
            {
                var attrs = (Synced[])prop.GetCustomAttributes(typeof(Synced), true);

                foreach (var attr in attrs)
                {
                    var obj = (INetSyncable)prop.GetValue(comp);
                    SyncDictionary[attr.SyncType].Add(prop, obj);
                    PrevSyncDictionary[attr.SyncType].Add(prop, obj.Clone());
                }
            }
        }

        private void RemoveComponentProps(Component comp)
        {
            var props = from p in comp.GetType().GetProperties()
                        let defined = p.IsDefined(typeof(Synced), true)
                        where defined
                        select p;
            foreach (var prop in props)
            {
                var attrs = (Synced[])prop.GetCustomAttributes(typeof(Synced), true);

                foreach (var attr in attrs)
                {
                    var obj = (ASyncable)prop.GetValue(comp);
                    SyncDictionary[attr.SyncType].Remove(prop);
                    PrevSyncDictionary[attr.SyncType].Remove(prop);
                }
            }
        }

        private void GameObj_EventComponentRemoving(object sender, ComponentEventArgs e)
        {
            var comp = e.Component;
            RemoveComponentProps(comp);
        }

        private void GameObj_EventComponentAdded(object sender, ComponentEventArgs e)
        {
            var comp = e.Component;
            AddComponentProps(comp);
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            var syncType = ESyncType.OnChanged;
            foreach (var pair in SyncDictionary[syncType])
            {
                var prev = PrevSyncDictionary[syncType][pair.Key];
                if (prev.Equals(pair.Value))
                    return;

                prev = pair.Value.Clone();
                //Sending changed packet
                _send(pair.Value);
            }
        }

        private void _send(INetSyncable prop)
        {
            prop.Serialize(Writer, Serializer);
            NetworkClient.Network.Res.Client.GetFirstPeer().Send(Writer, LiteNetLib.SendOptions.ReliableOrdered);
            Writer.Reset();
        }
    }
}