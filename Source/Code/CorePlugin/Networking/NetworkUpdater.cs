﻿using Duality;
using LiteNetLib.Utils;
using Xexen2d.Properties;

namespace Xexen2d.Networking
{
    public sealed class NetworkUpdater : Component, ICmpInitializable, ICmpUpdatable
    {
        public ContentRef<Network> Network { get; set; }

        public NetDataWriter Writer { get; private set; }
        public NetDataReader Reader { get; private set; }

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                Network = ContentProvider.RequestContent<Network>(Settings.Default.NetworkPath);
                Writer = new NetDataWriter();
                Reader = new NetDataReader();
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            Network.Res.Client.PollEvents();
        }
    }
}