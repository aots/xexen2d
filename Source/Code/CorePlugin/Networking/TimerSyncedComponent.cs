﻿using Duality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xexen2d.Networking
{
    public abstract class TimerSyncedComponent : NetworkClient, ICmpUpdatable
    {
        public float UpdatesPerSecond { get; set; } = 60f;

        private TimeSpan _lastTime = new TimeSpan(0);

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
        }

        public virtual void OnUpdate()
        {
            var currentTime = Time.MainTimer;
            if ((currentTime - _lastTime).TotalSeconds >= 1f / UpdatesPerSecond)
            {
                _lastTime = currentTime;
                DoNetUpdate();
            }
        }

        internal abstract void DoNetUpdate();
    }
}