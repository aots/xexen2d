﻿using Duality;
using Duality.Components;
using Xexen2d.Utils.Packets;

namespace Xexen2d.Networking
{
    [RequiredComponent(typeof(Transform))]
    public class PositionSync : Component
    {
        public Transform Transform { get; set; }

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
            if (context == InitContext.Activate)
            {
                Transform = GameObj.GetComponent<Transform>();
            }
        }

        internal override void DoNetUpdate()
        {
            var packet = Network.Res.Serializer.Serialize(
                new PositionPacket()
                {
                    X = Transform.Pos.X,
                    Y = Transform.Pos.Y
                });
            Writer.Put(packet);
            Network.Res.Send(Writer, LiteNetLib.SendOptions.ReliableOrdered);
        }
    }
}