﻿using Duality;
using Xexen2d.Attributes;
using Xexen2d.Utils.Packets;

namespace Xexen2d.Input
{
    public class KeyboardInput : Component, ICmpUpdatable
    {
        [Synced(ESyncType.OnChanged, ESyncDirection.ToServer)]
        private ControlStatePacket ControlState { get; set; } = new ControlStatePacket();

        [Synced(ESyncType.OnChanged, ESyncDirection.ToServer)]
        private GameClientStatePacket GameClientState { get; set; } = new GameClientStatePacket();

        public void OnUpdate()
        {
            if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.Escape))
            {
                GameClientState.IsExiting = true;
                return;
            }

            //if (DualityApp.Mouse.ButtonPressed(Duality.Input.MouseButton.Left))
            //{
            //    Network.Res.Serializer.Serialize(Writer, new PlayerShotPacket()
            //    {
            //        X = Transform.Pos.X,
            //        Y = Transform.Pos.Y,
            //        Angle = Transform.Angle
            //    });
            //    Network.Res.Client.GetFirstPeer().Send(Writer, LiteNetLib.SendOptions.ReliableOrdered);
            //    Writer.Reset();
            //}

            var controlState = new ControlStatePacket();

            if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.Space))
                controlState.Jumping = true;

            if (DualityApp.Mouse.ButtonPressed(Duality.Input.MouseButton.Left))
                controlState.Shooting = true;

            if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.A))
                controlState.ControlState = EControlState.MoveLeft;
            else if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.D))
                controlState.ControlState = EControlState.MoveRight;
            else
                controlState.ControlState = EControlState.Idle;

            if (!controlState.Equals(ControlState))
            {
                ControlState = controlState;
            }
        }
    }
}