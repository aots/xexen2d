﻿using System;
using Duality;
using LiteNetLib;
using LiteNetLib.Utils;
using Xexen2d.Networking;
using Xexen2d.Properties;

namespace Xexen2d
{
    /// <summary>
    /// Defines a Duality core plugin.
    /// </summary>
    public class Xexen2dCorePlugin : CorePlugin
    {
        public ContentRef<Network> Network { get; set; }

        // Override methods here for global logic

        protected override void OnGameStarting()
        {
            base.OnGameStarting();
            Network = ContentProvider.RequestContent<Network>(Settings.Default.NetworkPath);
        }
    }
}