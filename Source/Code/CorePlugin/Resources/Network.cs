﻿using Duality;
using LiteNetLib;
using LiteNetLib.Utils;

namespace Xexen2d.Networking
{
    [DontSerialize]
    public class Network : Resource
    {
        [DontSerialize]
        public EventBasedNetListener Listener = new EventBasedNetListener();

        [DontSerialize]
        public NetManager Client;

        //public NetSerializer Serializer { get; set; } = new NetSerializer();

        public string ConnectKey { get; set; } = "MyGame";
        public string IPAddress { get; set; } = "localhost";
        public int Port { get; set; } = 1488;
        //public EventHandler<ControlStatePacket> ControlStatePacketRecieved;

        //private void _onCStateRecv(ControlStatePacket p)
        //{
        //    ControlStatePacketRecieved?.Invoke(this, (ControlStatePacket)p.Clone());
        //}

        //public EventHandler<PositionPacket> PositionPacketRecieved;

        //private void _onPosRecv(PositionPacket p)
        //{
        //    PositionPacketRecieved?.Invoke(this, (PositionPacket)p.Clone());
        //}

        //public EventHandler<LoginResult> LoginResultRecieved;

        //private void _onLoginResultRecv(LoginResult p)
        //{
        //    var res = (LoginResult)p.Clone();
        //    LoginResultRecieved?.Invoke(this, res);
        //}

        //public EventHandler<DisconnectPacket> DisconnectRecieved;

        //private void _onDisconnectRecv(DisconnectPacket p)
        //{
        //    var res = (DisconnectPacket)p.Clone();
        //    DisconnectRecieved?.Invoke(this, res);
        //}

        //public EventHandler<PlayerShotPacket> PlayerShotPacketRecieved;

        //private void _onShotRecv(PlayerShotPacket p)
        //{
        //    var res = (PlayerShotPacket)p.Clone();
        //    PlayerShotPacketRecieved?.Invoke(this, res);
        //}

        protected override void OnLoaded()
        {
            base.OnLoaded();
            Client = new NetManager(Listener, ConnectKey);
            //Serializer.Register<LoginRequest>();
            //Serializer.Register<LoginResult>();
            //Serializer.Register<ControlStatePacket>();
            //Serializer.Register<PositionPacket>();
            //Serializer.Register<DisconnectPacket>();
            //Serializer.SubscribeReusable<ControlStatePacket, NetPeer>(OnControlStatePacketRecieved);
            //Serializer.SubscribeReusable<LoginResult, NetPeer>(OnLoginResultRecieved);
            //Serializer.SubscribeReusable<PositionPacket, NetPeer>(OnPositionRecieved);
            //Serializer.SubscribeReusable<DisconnectPacket, NetPeer>(OnDisconnectRecieved);
            //Serializer.SubscribeReusable<PlayerShotPacket, NetPeer>(OnPlayerShot);
        }

        //private void OnPlayerShot(PlayerShotPacket packet, NetPeer peer)
        //{
        //    _onShotRecv(packet);
        //}

        //private void OnDisconnectRecieved(DisconnectPacket packet, NetPeer peer)
        //{
        //    _onDisconnectRecv(packet);
        //}

        //private void OnPositionRecieved(PositionPacket p, NetPeer peer)
        //{
        //    _onPosRecv(p);
        //}

        //private void OnLoginResultRecieved(LoginResult packet, NetPeer peer)
        //{
        //    _onLoginResultRecv(packet);
        //}
        public void Start()
        {
            Client.Start();
        }

        public void Stop()
        {
            Client.Stop();
        }

        //private void Listener_NetworkReceiveEvent(NetPeer peer, NetDataReader reader)
        //{
        //    Serializer.ReadAllPackets(reader, peer);
        //}

        //private void OnControlStatePacketRecieved(ControlStatePacket packet, NetPeer peer)
        //{
        //    var p = (ControlStatePacket)packet.Clone();
        //    _onCStateRecv(p);
        //}

        public void Send(NetDataWriter writer, SendOptions opts)
        {
            Client.GetFirstPeer().Send(writer, opts);
        }

        public void Connect()
        {
            Client.Connect(IPAddress, Port);
        }
    }
}