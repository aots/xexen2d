﻿using Duality;
using Duality.Components;
using Duality.Resources;
using Xexen2d.Networking;
using Xexen2d.Utils.Packets;
using Xexen2d.Input;
using Xexen2d.Attributes;

namespace Xexen2d
{
    [RequiredComponent(typeof(Transform))]
    public class PlayerSpawner : Component, ICmpInitializable, ICmpUpdatable
    {
        public ContentRef<Prefab> Player { get; set; }

        [Synced(ESyncType.OnChanged, ESyncDirection.FromServer)]
        public LoginResult LoginResult { get; set; }

        [Synced(ESyncType.OnChanged, ESyncDirection.ToServer)]
        public LoginRequest LoginRequest { get; set; }

        private Transform _transform;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                _transform = GameObj.GetComponent<Transform>();
                LoginRequest = new LoginRequest();
                //Network.Res.Serializer.Serialize(Writer, new LoginRequest());
                //Network.Res.LoginResultRecieved += _loginRecieved;
                //Network.Res.Client.GetFirstPeer().Send(Writer, SendOptions.ReliableOrdered);
                //Writer.Reset();
            }
        }

        private void _spawnControllablePlayer(long id)
        {
            var player = Player.Res.Instantiate(_transform.Pos, _transform.Angle);
            player.GetComponent<NetSyncer>().ID = id;
            Scene.Current.AddObject(player);
        }

        private void _spawnRemotePlayer(long id)
        {
            var player = Player.Res.Instantiate(_transform.Pos, _transform.Angle);
            player.RemoveComponent<KeyboardInput>();
            player.GetComponent<NetSyncer>().ID = id;
            Scene.Current.AddObject(player);
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            if (LoginResult != null)
            {
                if (LoginResult.Yourself)
                {
                    _spawnControllablePlayer(LoginResult.ID);
                }
                else
                {
                    _spawnRemotePlayer(LoginResult.ID);
                }
                LoginResult = null;
            }
        }
    }
}