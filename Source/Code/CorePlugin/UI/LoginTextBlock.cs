﻿using Duality;
using Duality.Resources;
using SnowyPeak.Duality.Plugin.Frozen.UI.Widgets;
using Xexen2d.Networking;
using Xexen2d.Properties;

namespace Xexen2d.UI
{
    [RequiredComponent(typeof(ListBox))]
    public class LoginTextBlock : Component, ICmpInitializable
    {
        public ListBox ListBox { get; set; }
        public ContentRef<Scene> NextScene { get; set; }
        private ContentRef<Network> Network { get; set; }

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                Network = ContentProvider.RequestContent<Network>(Settings.Default.NetworkPath);
                ListBox = GameObj.GetComponent<ListBox>();
                Network.Res.Listener.PeerConnectedEvent += Listener_PeerConnectedEvent;
                Network.Res.Listener.NetworkReceiveEvent += Listener_NetworkReceiveEvent;
                Network.Res.Listener.NetworkErrorEvent += Listener_NetworkErrorEvent;
                Network.Res.Listener.PeerDisconnectedEvent += Listener_PeerDisconnectedEvent;
                Network.Res.Start();
                ListBox.Items.Clear();
                ListBox.Items.Add($"Connecting {Network.Res.IPAddress}:{Network.Res.Port}...");
                Network.Res.Connect();
            }
        }

        private void Listener_PeerDisconnectedEvent(LiteNetLib.NetPeer peer, LiteNetLib.DisconnectInfo disconnectInfo)
        {
            ListBox.Items.Add("Could not connect to server!");
        }

        private void Listener_NetworkErrorEvent(LiteNetLib.NetEndPoint endPoint, int socketErrorCode)
        {
            ListBox.Items.Add("Network error!");
        }

        private void Listener_PeerConnectedEvent(LiteNetLib.NetPeer peer)
        {
            ListBox.Items.Add("Connected!");
            Scene.SwitchTo(NextScene);
        }

        private void Listener_NetworkReceiveEvent(LiteNetLib.NetPeer peer, LiteNetLib.Utils.NetDataReader reader)
        {
            ListBox.Items.Add(reader.GetString(100));
        }

        public void OnShutdown(ShutdownContext context)
        {
            Network.Res.Listener.PeerConnectedEvent -= Listener_PeerConnectedEvent;
            Network.Res.Listener.NetworkReceiveEvent -= Listener_NetworkReceiveEvent;
            Network.Res.Listener.NetworkErrorEvent -= Listener_NetworkErrorEvent;
            Network.Res.Listener.PeerDisconnectedEvent -= Listener_PeerDisconnectedEvent;
        }
    }
}