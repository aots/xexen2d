﻿using Duality;
using SnowyPeak.Duality.Plugin.Frozen.UI.Widgets;
using Duality.Resources;
using Xexen2d.Networking;
using Xexen2d.Properties;
using Xexen2d.Attributes;

namespace Xexen2d.UI
{
    [RequiredComponent(typeof(TextBox))]
    public class StupidTextInput : Component, ICmpUpdatable, ICmpInitializable
    {
        public TextBox TextBox { get; set; }

        public ContentRef<Scene> NextScene { get; set; }
        public ContentRef<Network> Network { get; set; }

        public void OnInit(InitContext context)
        {
            Network = ContentProvider.RequestContent<Network>(Settings.Default.NetworkPath);
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            if (DualityApp.Keyboard.KeyHit(Duality.Input.Key.Enter))
            {
                if (TextBox.Text != "")
                    Network.Res.IPAddress = TextBox.Text;
                Scene.SwitchTo(NextScene);
            }

            if (DualityApp.Keyboard.KeyHit(Duality.Input.Key.BackSpace))
                if (TextBox.Text != "")
                    TextBox.Text = TextBox.Text.Substring(0, TextBox.Text.Length - 1);
                else
                    TextBox.Text = Network.Res.IPAddress;

            var inp = DualityApp.Keyboard.CharInput;
            TextBox.Text += inp;
        }
    }
}