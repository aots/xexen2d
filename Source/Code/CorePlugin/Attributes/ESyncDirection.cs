﻿namespace Xexen2d.Attributes
{
    public enum ESyncDirection : byte
    {
        ToServer,
        FromServer,
        TwoWay
    }
}