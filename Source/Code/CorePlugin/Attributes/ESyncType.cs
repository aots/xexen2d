﻿namespace Xexen2d.Attributes
{
    public enum ESyncType : byte
    {
        OnChanged,
        OnTimer
    }
}