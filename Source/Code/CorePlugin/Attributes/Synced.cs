﻿using System;

namespace Xexen2d.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class Synced : Attribute
    {
        public ESyncType SyncType { get; set; }
        public ESyncDirection SyncDirection { get; set; }

        public Synced(ESyncType syncType, ESyncDirection syncDirection)
        {
            SyncType = syncType;
            SyncDirection = syncDirection;
        }
    }
}