﻿using Duality;
using Duality.Components.Physics;
using Duality.Resources;
using Xexen2d.Attributes;

namespace Xexen2d.Bullets
{
    [RequiredComponent(typeof(RigidBody))]
    public class PlayerBullet : Component, ICmpUpdatable, ICmpInitializable
    {
        private float _spawnTime;
        public float LifeTime { get; set; } = 3f;
        public float BulletSpeed { get; set; } = 10f;
        private RigidBody _body;
        public Vector2 Direction { get; set; }

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                _body = GameObj.GetComponent<RigidBody>();
                _spawnTime = (float)Time.MainTimer.TotalSeconds;
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            if (Time.MainTimer.TotalSeconds >= _spawnTime + LifeTime)
            {
                Scene.Current.RemoveObject(GameObj);
                return;
            }

            _body.ApplyLocalForce(Direction * BulletSpeed);
        }
    }
}