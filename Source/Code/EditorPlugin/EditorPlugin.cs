﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Duality.Editor;

namespace Xexen2d.Editor
{
	/// <summary>
	/// Defines a Duality editor plugin.
	/// </summary>
    public class Xexen2dEditorPlugin : EditorPlugin
	{
		public override string Id
		{
			get { return "Xexen2dEditorPlugin"; }
		}
	}
}
