
--------------------[ TimeCounter ]---------------

Name                          Avg. Value    Min. Value    Max. Value         Samples 
Duality\Frame:                      6.13          0.00        604.00             449 
  Update:                           1.91          0.00        590.02             449 
    Physics:                        0.08          0.00         15.82             449 
      Contacts:                     0.00          0.00          0.76             449 
      Controller:                   0.03          0.00         13.34             449 
      Continous:                    0.01          0.00          5.38             449 
      AddRemove:                    0.00          0.00          0.03             449 
      Solve:                        0.03          0.00          7.86             449 
    Scene:                          1.38          0.00        524.48             449 
      All Components:               1.37          0.00        524.48             449 
      Transform:                    0.00          0.00          0.82             448 
      NetworkUpdater:               1.09          0.00        485.84             448 
      TextBox:                      0.86          0.00         39.59              46 
      StupidTextInput:              0.42          0.00         18.01              46 
      ListBox:                     36.96         36.96         36.96               1 
      RigidBody:                    0.01          0.00          3.74             401 
    Audio:                          0.03          0.00          6.80             449 
  Render:                           4.08          0.00        212.81             449 
    SwapBuffers:                    3.40          0.00         29.50             449 
    QueryVisibleRenderers:          0.02          0.00          4.52             449 
    CollectDrawcalls:               0.27          0.00         94.41             449 
      SpriteRenderer:               1.32          0.00         60.17              46 
      TextBox:                      0.80          0.01         34.10              45 
      ListBox:                      2.05          2.05          2.05               1 
      TilemapRenderer:              0.05          0.01         11.05             402 
    OptimizeDrawcalls:              0.00          0.00          0.01             449 
    ProcessDrawcalls:               0.24          0.00         47.83             449 
    PostProcessing:                 0.00          0.00          0.00               1 
  Log:                             45.13          3.21        154.67               4 
  VisualPicking:                    0.00          0.00          0.00               1 
  Unaccounted:                     -0.26       -154.67          7.74             449 
Cameras\Camera:                     0.68          0.06        183.33             448 

--------------------[ StatCounter ]---------------

Name                                          Avg. Value    Min. Value    Max. Value         Samples 
Duality\Stats\Audio\NumPlaying2D:                      0             0             0             449 
Duality\Stats\Audio\NumPlaying3D:                      0             0             0             449 
Duality\Stats\Render\NumDrawcalls:                     2             0             4             449 
Duality\Stats\Render\NumRawBatches:                    1             0             2             449 
Duality\Stats\Render\NumMergedBatches:                 1             0             2             449 
Duality\Stats\Render\NumOptimizedBatches:              1             0             2             449 
Duality\Stats\Memory\TotalUsage:                   41679         14428         45413             449 
Duality\Stats\Memory\GarbageCollect0:                 13             -             -               - 
Duality\Stats\Memory\GarbageCollect1:                  9             -             -               - 
Duality\Stats\Memory\GarbageCollect2:                  6             -             -               - 
Duality\Stats\Render\Tilemaps\NumTiles:              640           640           640             402 
Duality\Stats\Render\Tilemaps\NumVertices:           288           288           288             402 
