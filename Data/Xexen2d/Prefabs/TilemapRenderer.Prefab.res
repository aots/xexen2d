﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="1880805708">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2153259627">
      <_items dataType="Array" type="Duality.Component[]" id="2637370998" length="8">
        <item dataType="Struct" type="Duality.Components.Transform" id="1938082926">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">1880805708</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3" />
          <posAbs dataType="Struct" type="Duality.Vector3" />
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1415735196">
          <active dataType="Bool">true</active>
          <allowParent dataType="Bool">false</allowParent>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
          <continous dataType="Bool">false</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">false</fixedAngle>
          <gameobj dataType="ObjectRef">1880805708</gameobj>
          <ignoreGravity dataType="Bool">true</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="1331369048">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2714485420" length="0" />
            <_size dataType="Int">0</_size>
          </shapes>
        </item>
        <item dataType="Struct" type="Duality.Plugins.Tilemaps.Tilemap" id="1086451145">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">1880805708</gameobj>
          <tileData dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapData" id="3944683113" custom="true">
            <body>
              <version dataType="Int">3</version>
              <data dataType="Array" type="System.Byte[]" id="2397993230">H4sIAAAAAAAEAO3PsQ0AEABFwW8HA+jF/onpmEGHK16ufi1J3c2SAAAAAADwMuMSTv/6Jfz8twBYAYVJiBEAAA==</data>
            </body>
          </tileData>
          <tileset dataType="Struct" type="Duality.ContentRef`1[[Duality.Plugins.Tilemaps.Tileset]]">
            <contentPath dataType="String">Data\Xexen2d\hexen_tileset.Tileset.res</contentPath>
          </tileset>
        </item>
        <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollider" id="3543624591">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">1880805708</gameobj>
          <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
          <roundedCorners dataType="Bool">false</roundedCorners>
          <shapeFriction dataType="Float">0.3</shapeFriction>
          <shapeRestitution dataType="Float">0.3</shapeRestitution>
          <solidOuterEdges dataType="Bool">true</solidOuterEdges>
          <source dataType="Array" type="Duality.Plugins.Tilemaps.TilemapCollisionSource[]" id="3941866095">
            <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollisionSource">
              <Layers dataType="Enum" type="Duality.Plugins.Tilemaps.TileCollisionLayer" name="Layer0" value="1" />
              <SourceTilemap dataType="ObjectRef">1086451145</SourceTilemap>
            </item>
          </source>
        </item>
        <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapRenderer" id="2754838802">
          <active dataType="Bool">true</active>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <externalTilemap />
          <gameobj dataType="ObjectRef">1880805708</gameobj>
          <offset dataType="Float">0</offset>
          <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
          <tileDepthMode dataType="Enum" type="Duality.Plugins.Tilemaps.TileDepthOffsetMode" name="Flat" value="0" />
          <tileDepthOffset dataType="Int">0</tileDepthOffset>
          <tileDepthScale dataType="Float">0.01</tileDepthScale>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
      </_items>
      <_size dataType="Int">5</_size>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3665206472" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="1309212353">
          <item dataType="Type" id="26392238" value="Duality.Components.Transform" />
          <item dataType="Type" id="2854097610" value="Duality.Plugins.Tilemaps.TilemapRenderer" />
          <item dataType="Type" id="794035742" value="Duality.Plugins.Tilemaps.Tilemap" />
          <item dataType="Type" id="3776591834" value="Duality.Components.Physics.RigidBody" />
          <item dataType="Type" id="1943078478" value="Duality.Plugins.Tilemaps.TilemapCollider" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1138343648">
          <item dataType="ObjectRef">1938082926</item>
          <item dataType="ObjectRef">2754838802</item>
          <item dataType="ObjectRef">1086451145</item>
          <item dataType="ObjectRef">1415735196</item>
          <item dataType="ObjectRef">3543624591</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">1938082926</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="490819091">dWANtG+AeEihC1AGnneM3A==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">TilemapRenderer</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
