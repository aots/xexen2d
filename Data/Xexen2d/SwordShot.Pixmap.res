﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">0</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">0</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="16">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">15</H>
        <W dataType="Float">10</W>
        <X dataType="Float">19</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">11</H>
        <W dataType="Float">35</W>
        <X dataType="Float">72</X>
        <Y dataType="Float">3</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">11</H>
        <W dataType="Float">35</W>
        <X dataType="Float">121</X>
        <Y dataType="Float">3</Y>
      </item>
    </_items>
    <_size dataType="Int">3</_size>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAhgAAAARCAYAAACfFNXxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABHFSURBVHhe7Z3ZbxzHEYf7US8BmOiiRYrn8uYub4q3JOqydVq0Jcu0dVmyYlu2Jd9yHCuy4iM2jBx2LgN5y1uQBHkJEATIa/6tTX21XaPWiCJ3DpIben9AYWane6qrq6uqa3qOdXmhb7k9ovbjja70crcbudlXHr89yDasU2ZbRx3YiVC5famxPHSz28nWl6SH2WBoY9ifUR115A1sbd/7JTf90Uh54dMJfzRf9C2JTXuq23MdIYo3Cmp7ecbRmoMP6po8dC0168RBp8UJykM3elz30l7X/fRe13f+4eBfx/cXlUS0yx3/06I6BwGTAJoFoR0CnK30SiXZJemlTV+nboc5AH1OvlN0k++V3MJnk25C9r9PEx82tO/9IbW5xa+niXm+JD/QRtM7Z/CNctNrR93AxU6155FXxF/ObY4tM+5GdWwuBq52EOfKFkdHb/X6ki0ENXKSBzH4/gsd6nB0mCSDfTHEMg6SdQJJg9AZCABGGwmCLsQkV01A2Cw5NwoSjJVmPxrVwImtsJ2+k63fZoc9z7ZqQlt4RpJdcTrhpzZI8uv1v+kradhjLU7O+GjX6Wal0HfikDFzQ5K8oVfzdZ34NqkPK8lKgmmUJ7Bdxm7i7aLa0tLfjmucYyyz2O9KMHt1f/9rFEfRNdu+5zbOlkP9elJ/Uj28Nehmfjqi9mv9z1MHq0FlOS10stG1Htvlmhd2uKb92yM5DehoM2Q1OULKA8hcfKngBq5IgiFjsfzfJU1yx98ekAv6Fte+KG0JbRkQmNTovQPgdD6rV8conGrSOuuNcCAxIgyL9odkcmcJE3kYHMqM8gZtE6ALJyVQH5dJ7Xwb7ZSLL3VFAYF2Fz6d1Mnl6G8X3IzIpBOO0EbJudGwxOLwr2bZlg9+qb/VVpik5j950O+0MDtkzL2uKwkM+wTkZ4U2YSXNbBKqxcmZyaLwdLMGJ9Ofl1e3APmw5dLVLjf1wXC0+jT5XlEDHuWaRFfGQGm94WU0Uh0y0Y+83ucK55o0waRvg8uFzH4Ut1+zLWjxlzNu9t6Y2u/i1zP+jOzATvfeekrHhPbgT3vsD75YcIMXha4UfO31g+mXbfe5Fh3n4Vd7NZ6hC9FrmYnbbheNvLa+toxtEWM7T8gYI5eMs0yo5cbphnLz4g491rfcpjF2/v6Em7s3vimyhnqzLWOqsZ6Li0qs97WrB+cgOzzpEzZhie7Iq30kF+XRH2+h1QwGXDMnGWg6TEaljif7k+8WVdF5gMHBICbfLUUOP/lOyQ083+E6juxxLQd36UD2nm9142/qMyCqdOQYe7PfBsaCTaKgrkFTJiecelwy4QO/mHLHfr/gnvzugDskQWf2Z2Nu7M0B1/d8JakhKDDQBGWMneO9sj8sAz97d1Tbj3QkBmdymtGllXM1MA5k/J1P73E9y63KH7n3S6IT6rF5ervbM/Uj136s0Q280OHG3x6MZCrd6HZd53V1wHOtDn68ykd+M6eTE0Tf0JHw1d8zH4+4CZmwfF1/ZvVgjEhm0T88aQ/HY5/x8ORrZ0e1+ty7sFNl6jy5Z0MnZ+QjELOqw7L6/P3xR2y2dL1LZcP+VMbjTdEKBrrErznXbJbxQq/sm/2yz/n4p5c/Fz3Drxp/5zbs0Ms9Kh/yaF9Ev4UlvbApFy91aZmQ55wcvv3ysT/sj+wXu0IfxDuz4f2f74tkzQOmT+Mf2nPWPoVYVddHva6flHhwuXKbxuTBhg5+OeUWZCJHP+g+7aStfa0ixg680Kk6YezRf/tTja7t2G7Xdni36zzZ5IqXC7YqGI1TXrLiF9XG0KbZ7a7l8C7XceoJ13NB/Ii6H49GsR65hDzn6jHyRp/GZPpEHy2GKs8Ph7Vv+OyWAhPO3P0x7TQOZ8s2ZPxZgeERJIau9+jgmMGoIkXBamyi5MaRBjU6flM2fluM8VKnBqHStS5dUeg526q8Ste6ldZC1PZVaVv4WvvwRwaTg2Mqi5SbPCM3xKA+GnX7v9hXCeJynDrUt0AxIQlY8XpB5OwQAy26fWKkaeRcDTgFV3I95yUJxCkl+KpzeVmguB65GtA+yHGrh7ycx/n9VzoSJRpyvq7WoC8bP9pr6NsWjRd8STK8TP7MZKCvJLXwQMdmh/D2V+i+Znpk0aeVPWS/2EyOkzO8CJxDkgwiHzxNtrVsdkhsjXHiyi9+Lvo04lzKpySgSble2XaeqaxUGqWB6tYnRUn8nTqU8xubaj6wQxOk3rNtOmFl9SPhHdmv9Z8xpS1k4TiyHfyqkohQPw+gx9Gb/TpZmj3buA3KRIruswD+iWKrr2Pl2An1Ierq8yFif0lgMpQua5L9UBvGm33rN+Um18Byh5u5M7JqjM1DVrXLlD5vdUPdsmqddAUDWaHw2QvaC2OoyTEhCciWg3RYB9ASDPZxyrRAmQRKHCw0HPias9k2PM6W3xgCS4i9F1pd54k9unRkfNgK+ZYexVptG4Uy2DHqmAy2H5ZxHIIvhoKcXEmSZBROyaSdQM61IDyU1PlWcFyTKy6fOTOO0i9Z+ejtfjVqiDJ4+d++pbUhvPVKBMeCN47R0C6EgwQOiM2IDP6s5LBlbMYEgufM3ZHEqy4rIa0+ITvGOZyf9+QML13+/aDy8GFcttVkNJr+yYj6cCiv/TbiN3bJihz3vRsnGhzBVMhLkg4isxu8UrkggD82Yu0hR+hr4XGTj/OwpW2N28odJ/L1IyDtOFYwdPyEt9quJ/MLZMlqv3FM3RnSNsO+V+yvzddIDnSdJraGZRzDzg5/M+cW7k+6iVtysSSJAnwh2lgNJsPEW8VIBtNhXAaOQchibUMzcgFn+xy3MquXh6z4FZTU523ffrOlj7bKlRTISPtjt/o1vsErHkM5ntU2ahaiQHfu36ccgwqxz7E0YEB51dUG1AbJFMsxiP3wN/Xj5RyzoKVB51wlY35cwJFzqmo7bCPeZlgOHyMrg5fxtXOSyrkWhKfea4R32J61uRKZHGx10pAE44npH7rOZ/fofe3u5RY3eF2uCBPeKhHe4hQz7th3lSVmArIFZyYE2kKuA19OaT3qp4VdaZodnv/PaXfmL8d8aXpk1aedw/jmPTlXIxtlUFy+kMJjK9ms2qhPNAtnmh320XZot2s/3Oia5rZ7aZJD2sjk79QjoLPlXC3LyY+A8FO7ZILiAU8L6tguRDu0i+2RRFM/L3CrgH6ZPdMOV8ASK3yNZMiqa/ZN38gzd39cEyDPL9oX8i0+irgM1i/48jtOcVni+749JSuDn8mYVlbhlcnn1QbxF4nr+MrgpcqqE+PHuJIgPa7tOLiNKvwdq2TIsVIMHbs94BY+n3Q8d7jlIEp3z/zzRG4Jhg2qDaYZjG3NaMLf7Ft9BplB6OUZCNnn7QJuP0gw8K2sjGrajstgx6z9kDjfeNhv+EMmYxo510I1/Qjlt3omJ05BEM1DNmlbnRnetEdAtuBsWfjgxU51OupRPy2kjYcSXU++ND2y6jMc6zwnZ1CNbCvJyDGTLyQ/BlEZ+/C34+xrf3JKiKuRP5Tb6pl8oW4bGpwG8+4zLarblv07fSvpIfI8ZL8Ec7VdH+Bp12T1OvJnZgfPIVi70Nl/PKUrKWmRVde+f2oDh7+dcwe+ntLYYHXYt7fmHodQBmvH2uZY+BsK65gcIZlMVmbyQVlkDeU0viaXbUP5rJ7JgX/gG9gjca7nXGtiXRmk7cgGaWulGIoclAv5s7YQRGmPZNppb5EwsFkyRw02MgCFs83R1Xfvi61VXX1X0zZlULz9kMJjyGWyWZnx5nfPcktiOdeC8N20VaA4hI9eAR75dv6R7FsDtPDkuE8wlJJC5FTi3DDBYAXj3L9O+VrpAe9a0WccwrMq2UIZ4jKF5fAxCuvCD7mL10RWCYp53dKTczOvDiFXNPG3bMttdQhIe2q/rGDMfzaheragzhYZkPnJPx7QetTPCzzkCG+zZ68X1RmUFJyT11U5D1NKufJMclUelyGcnMM2IfsdHgvLbB/5TEYrg18WWamb52oPD2hauWyj/dVkMFAf2zr4VWVF63Ex9OSfD6e+sK9ZoCAGjY6jSDOYubvjmYImpIHYDzA8USJEGyHZ4FOPweUclqTSPD9Au1nvZ8/erTz0aueFshk/k4f28njOIQ7hofcYk95r1bYJ2JJ584GfvJ4P4R6/9R9eBGcmAevz/M/1CW9fOxnsyfG5T8YfCshe95E9ZQH6zPqcQN6Ts2GtsTYKZbRj1DEZbd9+My7wYsvvB/0ouAKvuMYepuS10DRgbKCk/g7ZMZv0u5f09XB9voUn+dGrUVpIOzopmlzomPbYIos/ntp+VwJjyhs81kezZ94IYkz8JO1rV4+0urZ9+80WOdI8V0D7xFieRTIZjDft0NeQQlmMqomxYXkaWdfyK5ONbXgcQgb6hp6JczP3Rt3wzV7XL/GUFxCqeQYkDvRm+lophgo5/1kEf8b/OeiIJ+0gikXRqlyCZuWjML52MsjApH56NzQGlYWBlvOTvAGB3BhAmifyebr5wBe8FjWp2TJf/jPZor54flnlXAvSjl4hJ30TZ+DFDtd3ofLu9kpvuCSdTPxVeuR8Ok5+POkzbQr52sngr1CUL3xCp5djOCbt+NrpoTaZ8U2HvCfnENJWZaxTvPk0fWdY3wY79OtZvXLe967YLH3AZr3vGS/2OZbn66Cq2xp9Q0d46kRvOoBol3bYtytL6uUF+36O9cvsmd+2FfK1kyGLrkNbMH1zgZnm2w6MDzF2+JVelYP2TIa17HXstX4382FlNeLIN/OOtyd0wo3L6nlmkRXbSRRDfR2LQ1YXeSjnOQn6nhSMGefDD/60SVsqx+uaOFZ0dH5rJRj6xgZb67h28gH52ukgCq36/ePmef2aW5lJwN6JxpCy3HKg/bW+KRB+B6NwvEmfU8AoMTyMovnoDtdydKdrObTL8e525xnpy3OtrnS1O5dvTVQDxgFnfty3BfbO73Sti7sdr3/FvyWSxzc6Sle63ZRMYsJD7cR4s+V1XT53i0xpwFIm3xlB38Y/vIrAPrnfmSfW0udGfqshDrW9Kr4rwOuOPUutlWdr8DMCoNge+zyI2rIoNsv7/Kf3uL6L7cpLbWAdPmhlUDkyfGOE1525MmbS0rhU8UOltNCxfa8ULZkzbowlRLu8KomPUC8PYFul65XP3VtbZs983ZPjaVcwQlSr64e+7XCy8m2H0Tf6c/m2A0COamIs9sqzYIx7+xGRnSRCYi3nN840uMapBrdz6Af6LZ+9YrvYBnF27OZAtFqSRVbG5bE+778Z0npYYmjsmyFHfjfv5u6Nif8McI7GAPgkRfupRscFCufDl+SFNtADesPfsZetlmC47udk0MW56Syd54rMl/Hb115/qMM8ILJFX7L+CNsGFtQ06yRwiyNvtExpQaDhCoptXl8Z5Vx1BtGBnKuOjjMMXdOrAl8rOTiX5XBvaxHhgGyLVypXgkL+jI2B2YInTSDW62uTeUDkeUBeZrVZbHeFhDesvxkIdKv3ndf7K6lmv8Q2eNMOuiGh4TjleQFbxWbhSxtsac8mp673j2T6tkcaeF1r/LItcmb9OmVS+Pb1Y4CaXPgEw2R6HNZb1lAGbqVyQUMSxngxbthM1i+Jen9TG4CHxdDh673hRa3SlgIdtz+YwqkZTE8bHtiBH2ylzYY3Ctd+pnZkSgqc0SgtOJfv6PdfrnwsBgch8+a2SxaYnYleyz0kGhfUwZQ/W7tNtxl2CGzMw3GPbEKoFhHJLFdMtSxnCOSNJhC5AvfP5fjS7DD7DyeNyTtF13elLbNvxGE2PSj+wn174ir2LH2sJM5i13zMbyMR2URAtYJakCmUwVPu/9uCH7LCrbe2JHZig5pQf5iv/dUkUJ4RsKAO1VGHASchw9cgKYGTT6pnAfaFI+tKkTg22TvLhfDWL/ZttWy+jk0DsY1bcTrZS5K8Xv9iGcZO2uTWDPYc3gYceqnH166j1hAmGusBVisGX+jUJANb8ORL66jjew4mfZ498M8f+KP5AZ5GddSRF7xN6QqCvlG0QSs7ZsvcFuFZJtrf6BWMOmoLJBnFSwV98JTkojYSDOf+B2xukeRB8tVfAAAAAElFTkSuQmCC</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
