﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="3558706846">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3429875720">
        <_items dataType="Array" type="Duality.Component[]" id="1135006060" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="3615984064">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">3558706846</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-500</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-500</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="810126027">
            <active dataType="Bool">true</active>
            <farZ dataType="Float">10000</farZ>
            <focusDist dataType="Float">500</focusDist>
            <gameobj dataType="ObjectRef">3558706846</gameobj>
            <nearZ dataType="Float">0</nearZ>
            <passes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Camera+Pass]]" id="4238833991">
              <_items dataType="Array" type="Duality.Components.Camera+Pass[]" id="2874598094" length="4">
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="344048592">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="All" value="3" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="PerspectiveWorld" value="0" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="AllGroups" value="2147483647" />
                </item>
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="2848068206">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="None" value="0" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="OrthoScreen" value="1" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
            </passes>
            <perspective dataType="Enum" type="Duality.Drawing.PerspectiveMode" name="Parallax" value="1" />
            <priority dataType="Int">0</priority>
            <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="952766942" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1617567946">
            <item dataType="Type" id="3525565280" value="Duality.Components.Transform" />
            <item dataType="Type" id="3217229966" value="Duality.Components.Camera" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1472216730">
            <item dataType="ObjectRef">3615984064</item>
            <item dataType="ObjectRef">810126027</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3615984064</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="803581994">4/Uf7ORRYUSvTHXwS5dGaQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Camera</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3401797388">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2654421250">
        <_items dataType="Array" type="Duality.Component[]" id="1477671312" length="8">
          <item dataType="Struct" type="Duality.Components.Transform" id="3459074606">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3401797388</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2936726876">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3401797388</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.Tilemap" id="2607442825">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3401797388</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollider" id="769648975">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3401797388</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapRenderer" id="4275830482">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3401797388</gameobj>
          </item>
        </_items>
        <_size dataType="Int">5</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1685043082" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="600278232">
            <item dataType="ObjectRef">3525565280</item>
            <item dataType="Type" id="632479660" value="Duality.Plugins.Tilemaps.TilemapRenderer" />
            <item dataType="Type" id="3114126262" value="Duality.Plugins.Tilemaps.Tilemap" />
            <item dataType="Type" id="966369272" value="Duality.Components.Physics.RigidBody" />
            <item dataType="Type" id="4265074066" value="Duality.Plugins.Tilemaps.TilemapCollider" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="943485086">
            <item dataType="ObjectRef">3459074606</item>
            <item dataType="ObjectRef">4275830482</item>
            <item dataType="ObjectRef">2607442825</item>
            <item dataType="ObjectRef">2936726876</item>
            <item dataType="ObjectRef">769648975</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3459074606</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2876284036">r+xGgvRPwkaVCJT+pnYT5A==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">TilemapRenderer</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1393402738">
        <changes />
        <obj dataType="ObjectRef">3401797388</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Xexen2d\Prefabs\TilemapRenderer.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="4021656959">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1110133117">
        <_items dataType="Array" type="Duality.Component[]" id="3457645350" length="4">
          <item dataType="Struct" type="Xexen2d.Networking.NetworkUpdater" id="3774156622">
            <_x003C_Network_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Xexen2d.Networking.Network]]">
              <contentPath dataType="String">Data\Xexen2d\Resources\Network.Network.res</contentPath>
            </_x003C_Network_x003E_k__BackingField>
            <_x003C_Reader_x003E_k__BackingField dataType="Struct" type="LiteNetLib.Utils.NetDataReader" id="2576319314">
              <_data />
              <_dataSize dataType="Int">0</_dataSize>
              <_position dataType="Int">0</_position>
            </_x003C_Reader_x003E_k__BackingField>
            <_x003C_Writer_x003E_k__BackingField dataType="Struct" type="LiteNetLib.Utils.NetDataWriter" id="440444618">
              <_autoResize dataType="Bool">true</_autoResize>
              <_data dataType="Array" type="System.Byte[]" id="3733361800">AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</_data>
              <_maxLength dataType="Int">64</_maxLength>
              <_position dataType="Int">0</_position>
            </_x003C_Writer_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4021656959</gameobj>
          </item>
        </_items>
        <_size dataType="Int">1</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3920545208" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3121864215">
            <item dataType="Type" id="2034551566" value="Xexen2d.Networking.NetworkUpdater" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1386595520">
            <item dataType="ObjectRef">3774156622</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1217477557">gVZUi5zXCkC8KIxeibrFGg==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">NetworkUpdater</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1442052474">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1477430884">
        <_items dataType="Array" type="Duality.Component[]" id="4072957380" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="1499329692">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1442052474</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2450021">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1442052474</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="976981962">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1442052474</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3101343766" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3809206574">
            <item dataType="ObjectRef">3525565280</item>
            <item dataType="Type" id="1789028176" value="Duality.Components.Renderers.AnimSpriteRenderer" />
            <item dataType="ObjectRef">966369272</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1061069002">
            <item dataType="ObjectRef">1499329692</item>
            <item dataType="ObjectRef">2450021</item>
            <item dataType="ObjectRef">976981962</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1499329692</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="416187550">O0+k7ejppkGE5APM8CH2wQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">mage</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1504895840">
        <changes />
        <obj dataType="ObjectRef">1442052474</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Xexen2d\Prefabs\mage.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
